# Description
  Building Ubuntu Focal Fossa 20.04 LTS desktop vagrant boxes using packer

# Builds

* the builds using a docker `apt_cacher_ng` image to speed up iterations.
* build and start it with following command:

```
docker build -t apt-cacher . && docker run -d -p 3142:3142 --name apt-cacher-run apt-cacher
```

* disable or change following line according to your environment:

```
d-i mirror/http/proxy string http://192.168.5.57:3142/
```


## Hyper-V Desktop

```
packer build -force -only hyperv-iso -except vagrant-cloud templates/desktop.json
```

## Hyper-V Server

You have to ensure that oscdimg.exe as part of the Windows Assessment and Deployment Kit ist available and in current PATH variable:

for example: 
```
$env:PATH = $env:PATH + ";C:\Program Files (x86)\Windows Kits\10\Assessment and Deployment Kit\Deployment Tools\amd64\Oscdimg"
```

```
packer build -force -only hyperv-iso -except vagrant-cloud templates/server.json
```

## Virtualbox Desktop

```
packer build -force -only virtualbox-iso -except vagrant-cloud templates/desktop.json
```

## Virtualbox Server

```
packer build -force -only virtualbox-iso -except vagrant-cloud templates/server.json
```

# Vagrant Cloud Boxes
  - https://app.vagrantup.com/sn0wf4k3

# Details
  - hyper-v generation 2 vm with enhanced session mode enabled (uefi partition scheme)
  - virtualbox with uefi firmeware and sata disk
